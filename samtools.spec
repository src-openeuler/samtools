Name:    samtools
Version: 1.21
Release: 1
Summary: Tools (written in C using htslib) for manipulating next-generation sequencing data 
License: MIT and BSD
URL:     http://www.htslib.org
Source0: https://github.com/samtools/samtools/archive/%{name}-%{version}.tar.bz2

BuildRequires: gcc autoconf automake make zlib-devel htslib-devel ncurses-devel bzip2-devel xz-devel

%description
The original samtools package has been split into three separate but tightly coordinated projects:
    htslib: C-library for handling high-throughput sequencing data
    samtools: mpileup and other tools for handling SAM, BAM, CRAM
    bcftools: calling and other tools for handling VCF, BCF
See also http://github.com/samtools/

%package    devel
Summary:    Header files and libraries for compiling against %{name}
Group:      Development/Libraries
Requires:   %{name} = %{version}-%{release}

%description devel
Header files and libraries for compiling against %{name}

%prep
%autosetup

%build
autoheader
autoconf -Wno-syntax
%configure --prefix=%{_prefix} --libdir=%{_libdir} --with-htslib 
%make_build

%install
%make_install
install -d -m 0755 %{buildroot}%{_includedir}/bam
install -p -m 0644 *.h %{buildroot}%{_includedir}/bam

%files
%license LICENSE 
%doc README INSTALL doc/ examples/
%{_bindir}/*
%{_mandir}/*

%files devel
%defattr(-,root,root,-)
%{_includedir}/bam

%changelog
* Sat Nov 23 2024 wangxiaomeng <wangxiaomeng@kylinos.cn> - 1.21-1
- update version to 1.21
  - New work and changes: 
    -`samtools reset` now removes a set of predefined auxtags, as these tags are no longer valid after the reset operation.  This behaviour can be overridden if desired.
    - `samtools reset` now also removes duplicate flags. 
    - Added a report of the number of alignments for each primer to `samtools ampliconclip`
  - Bug fixes:
    - Avoid `tview` buffer overflow for positions with >= 14 digits.
    - Fixed hard clip trimming issue in `ampliconclip` where right-hand side qualities were being removed from left-hand side trims.
    - Fixed a bug in `samtools merge --template-coordinate` where the wrong heap was being tested.

* Tue Jun 04 2024 kywqs <weiqingsong@kylinos.cn> - 1.20-1
- update version to 1.20
- Fix a major bug when searching against a CRAM index where one container has start and end coordinates entirely contained within the previous container.
- Fixed bug introduced in release 1.4 that caused incorrect reference bases to be printed by samtools mpileup -a -f ref.fa in the zero-depth regions at the end of each reference.

* Thu May 25 2023 guoyizhang <kuoi@bioarchlinux.org> - 1.17-1
- update: 1.17

* Thu Mar 10 2022 herengui <herengui@uniontech.com> - 1.12-5
- fix build error

* Tue Feb 08 2022 herengui <herengui@uniontech.com> - 1.12-4
- rebuild for htslib version updated.

* Tue Jul 20 2021 herengui <herengui@uniontech.com> - 1.12-3
- remove useless buildrequires.
- replace tab with space

* Thu Mar 25 2021 herengui <herengui@uniontech.com> - 1.12-2
- Add devel package

* Thu Mar 18 2021 yangzhao <yangzhao1@kylinos.cn> - 1.12-1
- Package init

